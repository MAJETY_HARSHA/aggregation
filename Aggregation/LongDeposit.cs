namespace Aggregation
{
    public class LongDeposit : Deposit
    {
        public LongDeposit(decimal amount, int period) : base(amount, period)
        {

        }

        public override decimal Income()
        {
            var income = Amount;
            for (int i = 6; i < Period; i++)
            {
                income += income * (decimal)0.15;
            }
            return income - Amount;
        }
    }
}