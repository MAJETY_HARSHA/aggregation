namespace Aggregation
{
    public class Client
    {
        private readonly Deposit[] deposits;

        public Client()
        {
            deposits = new Deposit[10];
        }

        public bool AddDeposit(Deposit deposit)
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] == null)
                {
                    deposits[i] = deposit;
                    return true;
                }
            }
            return false;
        }

        public decimal TotalIncome()
        {
            decimal totalIncome = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != null)
                {
                    totalIncome += deposits[i].Income();
                }
            }
            return totalIncome;
        }

        public decimal MaxIncome()
        {
            decimal total = deposits[0].Income();

            foreach (Deposit depo in deposits)
            {
                if (depo != null && depo.Income() > total)
                {
                    total = depo.Income();
                }
            }
            return total;
        }

        public decimal GetIncomeByNumber(int number)
        {
            if (deposits[number - 1] != null)
                return deposits[number - 1].Income();
            else
                return 0;
        }
    }
}