namespace Aggregation
{
    //TODO: Define public class "BaseDeposit" that inherits from "Deposit".

    public class BaseDeposit : Deposit
    {
        public BaseDeposit(decimal amount, int period) : base(amount, period)
        {

        }

        public override decimal Income()
        {
            int months = 0;
            decimal  increased=amount;
            while (months < period)
            {
                months++;
                increased+= System.Math.Round((increased/100*5),2);
            }
            return increased - amount;
        }
    }
    //TODO: Define constructor that calls constructor of a base class.

    //TODO: Override method "Income" of base class according to the task. 
}