namespace Aggregation
{
    //TODO: Define public abstract class "Deposit"
    public abstract class Deposit{
        public readonly decimal amount;
        public readonly int period;
        public decimal Amount { get { return amount; } }
        public int Period { get { return period; } }
        public Deposit(decimal amount, int period)
        {
            this.amount = amount;
            this.period = period;
        }
        public abstract decimal Income();
    }
    
}