namespace Aggregation
{
    public class SpecialDeposit : Deposit
    {
        private static decimal percent = (decimal)0.01;
        public SpecialDeposit(decimal amount, int period) : base(amount, period)
        {

        }
        public override decimal Income()
        {
            var income = Amount;
            var incomePercent = percent;
            for (int i = 0; i < Period; i++)
            {
                income += income * incomePercent;
                incomePercent += percent;
            }
            return income - Amount;
        }
    }
}